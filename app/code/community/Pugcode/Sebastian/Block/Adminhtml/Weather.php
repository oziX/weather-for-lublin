<?php
class Pugcode_Sebastian_Block_Adminhtml_Weather extends Mage_Adminhtml_Block_Widget_Grid_Container {
    
    public function __construct() {
        $this->_blockGroup = 'pugcode';
        $this->_controller = 'adminhtml_weather';
        $this->_headerText = $this->__('Pugcode | Weather');
        parent::__construct();
        $this->_removeButton('add');
    }
}