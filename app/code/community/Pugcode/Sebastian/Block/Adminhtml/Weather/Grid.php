<?php
class Pugcode_Sebastian_Block_Adminhtml_Weather_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    
    public function __construct() {
        parent::__construct();
        $this->setId('pugcodesebastian_weather_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
    }
    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('pugcode_sebastian/weather_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => 'LOG ID',
            'index' => 'entity_id'
        ));
        $this->addColumn('temperature', array(
            'header' => 'Temperature',
            'index' => 'temperature'
        ));
        $this->addColumn('created_at', array(
            'header' => 'Date',
            'index' => 'created_at'
        ));
        return parent::_prepareColumns();
    }
}