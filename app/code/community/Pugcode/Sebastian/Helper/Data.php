<?php
class Pugcode_Sebastian_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * get api key
     */
    public function getKey()
    {
        return Mage::getStoreConfig('pugcode_weather/custom_auth/auth');
    }
}