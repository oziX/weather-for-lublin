<?php
/**
 * Class Pugcode_Sebastian_Model_Resource_Weather
 */
class Pugcode_Sebastian_Model_Resource_Weather extends Mage_Core_Model_Resource_Db_Abstract {
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('pugcode_sebastian/weather', 'entity_id');
    }
}