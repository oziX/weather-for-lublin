<?php
/**
 * Class Pugcode_Sebastian_Model_Resource_Weather_Collection
 */
class Pugcode_Sebastian_Model_Resource_Weather_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection constructor
     */
    public function _construct()
    {
        $this->_init('pugcode_sebastian/weather');
    }
}