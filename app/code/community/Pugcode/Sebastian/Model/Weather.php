<?php
class Pugcode_Sebastian_Model_Weather extends Mage_Core_Model_Abstract
{

    public static $url = "https://api.openweathermap.org/data/2.5/weather?q=Lublin&units=metric&appid="; 

    public function _construct()
    {
        $this->_init('pugcode_sebastian/weather');
    }

    /**
     * Add weather to database
     */
    public function addWeather($temperature)
    {
        $this->setTemperature($temperature);
        $this->setCreatedAt(Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s'));
        $this->save();
    }

    /**
     * Cron action
     */
    public function whatWeather()
    {
        $apiKey = Mage::helper('pugcode')->getKey();
        $weather = $this->useApi($apiKey);
        $weatherObject = json_decode($weather);
        $this->addWeather($weatherObject->main->temp);
    }

    /**
     * curl communication
     */
    public function useApi($apikey)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::$url . $apikey,
            CURLOPT_USERAGENT => 'pugcode'
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }

    /**
     * get last weather log
     */
    public function lastWeather()
    {
        $weatherCollection = Mage::getResourceModel('pugcode_sebastian/weather_collection');
        return $weatherCollection->getLastItem()->getTemperature();
    }
}

