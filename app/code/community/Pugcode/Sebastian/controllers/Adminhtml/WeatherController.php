<?php
class Pugcode_Sebastian_Adminhtml_WeatherController extends Mage_Adminhtml_Controller_Action {
    
    public function indexAction() {
        $this->_title($this->__('System'))
            ->_title($this->__('Pugcode | Weather'))
            ->_title($this->__('Weather'));
        $this->loadLayout();
        $block =  $this->getLayout()->createBlock('pugcode/adminhtml_weather');
        $this->_addContent($this->getLayout()->createBlock($block));
        $this->renderLayout();
    }
}