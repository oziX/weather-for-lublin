<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$conn = $installer->getConnection();
$table = $installer->getTable('pugcode_sebastian/weather');
$orderTable =
    $conn
        ->newTable($table)
        ->addColumn(
            'entity_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            [
                'auto_increment' => true,
                'unsigned'       => true,
                'nullable'       => false,
                'primary'        => true,
            ],
            'entity_id'
        )
        ->addColumn(
            'temperature',
            Varien_Db_Ddl_Table::TYPE_TEXT,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
            ],
            'Temperature'
        )
        ->addColumn(
            'created_at',
            Varien_Db_Ddl_Table::TYPE_DATETIME,
            null,
            [
                'nullable' => true
            ],
            'Creation date'
        );
$conn->createTable($orderTable);